#!/usr/bin/env python3
# coding: utf-8
###################################################################################################
#  Copyright Jonathan Lucas (c) 2020.
###################################################################################################
#
#    This file is part of cases.
#
#    cases is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    cases is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with cases.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################################

###################################################################################################
# Imports
###################################################################################################

import argparse as ap
import datetime
import logging
import os
import sys

import matplotlib.pyplot as plt
import pandas as pd

from data import POPULATION
from utils import (
    read_uk_ons_data,
)

plt.rcParams["figure.figsize"] = (8.27, 11.69)

###################################################################################################
# Globals
###################################################################################################

LOG = logging.getLogger(__file__)
ONE_DAY = pd.Timedelta("1D")


def plot_absolute(ax, full_data, marker, sub_label, plot_worm, tgt, countries):
    data = full_data["daily_est"]
    for country in countries:
        col = ax.plot(data[country].iloc[1:], marker, label=f"{country} {sub_label}", alpha=0.1)[0].get_color()

    with pd.option_context("mode.chained_assignment", None):
        if plot_worm and len(tgt):
            uk_data = data.loc[:, ["UK"]]
            uk_data["cum"] = uk_data.cumsum()
            tgt["target_date"] = tgt.index
            tgt = uk_data.join(tgt, how="outer")

            tgt.target.bfill(inplace=True)
            tgt.target_date.bfill(inplace=True)
            tgt["required"] = tgt.target - tgt.cum.ffill()

            tgt.target[tgt.required < 0] = pd.NA
            tgt.target_date[tgt.required < 0] = pd.NA
            tgt.target.bfill(inplace=True)
            tgt.target_date.bfill(inplace=True)

            tgt["required"] = tgt.target - tgt.cum
            tgt["days_left"] = (tgt.target_date - tgt.index) / ONE_DAY
            tgt["RRR"] = tgt.required / tgt.days_left
            tgt["RRR"][tgt["RRR"] < 0] = 0
            ax.plot(tgt["RRR"], "--", color=col, label=f"UK {sub_label} RRR")

    rolling = data["UK"].rolling("7D").mean()
    rolling = rolling.shift(-3, "D")[4:]
    ax.plot(rolling, ls="-", color=col, label=f"UK {sub_label} 7-day mean")


def plot_cumulative(ax, full_data, marker, sub_label, plot_worm, target_val, countries):
    data = full_data["cum"]
    for country in countries:
        country_data = data[country].dropna()
        col = ax.plot(country_data, marker, label=f"{country} {sub_label}", alpha=0.1)[0].get_color()

    if plot_worm and len(target_val):
        ax.plot(target_val, "--x", color=col, label=f"UK {sub_label} RRR")

    rolling = full_data["cum"]["UK"].rolling("7D").mean()
    rolling = rolling.shift(-3, "D")[4:]
    ax.plot(rolling, ls="-", color=col, label=f"UK {sub_label} 7-day mean")


def preprocess_data(full_data, data_idx, args):
    daily = full_data.loc[:, (f"daily{data_idx}",)].dropna(how="all")
    daily["UK"] = daily.sum(axis=1)
    per_diem = daily.diff()
    per_diem.iloc[0] = daily.iloc[0]
    combined_data = pd.concat(
        [per_diem, daily, per_diem], axis=1, keys=["raw", "cum", "daily_est"]
    )
    # combined_data.loc[:,("cum",slice(None))]  = combined_data.loc[:,("cum",slice(None))].ffill()
    return combined_data


def plot_cases(full_data, args):
    try:
        target_vals = pd.DataFrame(
            {"target": args.target}, index=pd.to_datetime(args.target_date)
        )
        try:
            data = preprocess_data(full_data, "CumVaccinatedFirstDose", args)
            # target_vals2 = target_vals.copy()
            target_vals2 = data["cum", "UK"]
            target_vals2 = target_vals2[target_vals2 > 0]
            target_vals2 = target_vals2.resample(f"{args.second_target_period}D").mean()
            target_vals2.index += ONE_DAY * \
                (args.second_target_offset + args.second_target_period // 2)
            target_vals2.at[pd.to_datetime(target_vals2.index[0] - args.second_target_period * ONE_DAY)] = 0
            target_vals2.sort_index(inplace=True)
            target_vals2.name = "target"
            target_vals2 = pd.DataFrame(target_vals2)

            target_vals3 = pd.concat([target_vals, target_vals2], axis=1)
            target_vals3 = target_vals3.resample("1D").first()
            target_vals3 = target_vals3.interpolate("linear")
            target_vals3 = target_vals3.loc[target_vals2.index.append(target_vals.index), :].sort_index()
            #            target_vals3 = target_vals3.resample(f"{args.second_target_period}D").first()
            target_vals3 = target_vals3.sum(axis=1)
            target_vals3.name = "target"
            target_vals3 = pd.DataFrame(target_vals3)

        except (AttributeError, TypeError):
            target_vals2 = pd.DataFrame()
            target_vals3 = pd.DataFrame()
    except ValueError:
        target_vals = target_vals2 = target_vals3 = pd.DataFrame()

    fig, ax = plt.subplots(3, 1, sharex="col")
    if args.combined:
        plot_types = (
            ("combined dose", "CombinedDoses", "-", True, target_vals3),
        )
    else:
        plot_types = (
            ("1st dose", "CumVaccinatedFirstDose", "-", True, target_vals),
            ("2nd dose", "CumVaccinatedSecondDose", "-", True, target_vals2),
        )

    for sub_label, data_idx, marker, plot_worm, tgt in plot_types:
        if data_idx == "CombinedDoses":
            data1 = preprocess_data(full_data, "CumVaccinatedFirstDose", args)
            data2 = preprocess_data(full_data, "CumVaccinatedSecondDose", args)
            data = data1 + data2
        else:
            data = preprocess_data(full_data, data_idx, args)
        countries = args.country or set(data.columns.droplevel(0))

        plot_absolute(
            ax[0],
            data / 1e6,
            marker,
            sub_label,
            plot_worm,
            tgt / 1e6,
            countries,
        )
        plot_cumulative(
            ax[1],
            data / 1e6,
            marker,
            sub_label,
            plot_worm,
            tgt / 1e6,
            countries,
        )
        plot_cumulative(
            ax[2],
            data.divide(POPULATION / 100, level=1),
            marker,
            sub_label,
            plot_worm,
            tgt / POPULATION["UK"] * 100,
            countries,
        )

    last_date = full_data.index[-1].date()
    ax[0].set_title(f"UK vaccinations per day by country as of {last_date}")
    ax[0].grid(True)
    ax[0].set_ylabel("Millions")

    ax[1].set_title("UK cumulative vaccinations by country")
    ax[1].grid(True)
    ax[1].set_ylabel("Millions")

    ax[2].set_title("UK population percentage vaccinated by country")
    ax[2].tick_params(axis="x", rotation=30)
    ax[2].grid(True)
    ax[2].set_ylabel("%age of population")

    ax[2].set_xlabel("Date")
    ax[0].legend(loc="best", fontsize="x-small")
    plt.tight_layout()

    return fig


def run_script():
    parser = ap.ArgumentParser(
        description="Plot UK COVID-19 vaccination progress.",
        formatter_class=ap.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-v",
        "--verbosity",
        type=int,
        choices=(0, 1, 2, 3, 4),
        default=2,
        help="Set verbosity level for debug (0=silent, 4=maximum chattiness)",
    )
    parser.add_argument(
        "-i",
        "--input_data",
        type=str,
        default="data/uk-nation.csv",
        help="Input data file.",
    )
    parser.add_argument(
        "-f", "--format", action="store", default="svg", help="Destination format."
    )
    parser.add_argument(
        "-t",
        "--target",
        type=float,
        nargs="*",
        help="Set target vaccination numbers",
    )
    parser.add_argument(
        "-d",
        "--target_date",
        nargs="*",
        type=datetime.date.fromisoformat,
        help="Set target vaccination numbers",
    )
    parser.add_argument("-t2o", "--second_target_offset", type=int,
                        help="If set, indicates that the target for 2nd vaccinations is implictly "
                             "T2O days after the 1st one.")

    parser.add_argument("-t2p", "--second_target_period", type=int, default=14,
                        help="Indicates how often to generate a new target from the old one")

    parser.add_argument("-o", "--output", type=str, help="Output directory.")
    parser.add_argument("-C", "--country", nargs="*", help="Plot this country.")
    parser.add_argument("-c", "--combined", action="store_true", help="Plot combined vaccinations")
    args = parser.parse_args()

    log_levels = [
        logging.CRITICAL,
        logging.ERROR,
        logging.WARNING,
        logging.INFO,
        logging.DEBUG,
        logging.NOTSET,
    ]
    logging.basicConfig(
        format="%(asctime)s.%(msecs)03d %(name)-12s %(levelname)-8s %(message)s",
        datefmt="%m-%d %H:%M:%S",
        # level=log_levels[args.verbosity],  # uncomment for 3rd party msgs
        stream=sys.stderr,
    )
    LOG.setLevel(log_levels[args.verbosity])
    LOG.debug("Command line arguments processed: %s", args)

    data, _, _ = read_uk_ons_data(
        args.input_data,
        values=[
            "weeklyCumVaccinatedFirstDose",
            "weeklyCumVaccinatedSecondDose",
            "dailyCumVaccinatedFirstDose",
            "dailyCumVaccinatedSecondDose",
            "cases",
            "deaths",
        ],
    )

    # plot_cases(data)
    fig = plot_cases(data, args)
    if fig:
        if args.output:
            if args.combined:
                filename = os.path.join(
                    args.output, f"UK_vaccination_progress_combined.{args.format}"
                )
            else:
                filename = os.path.join(
                    args.output, f"UK_vaccination_progress.{args.format}"
                )

            LOG.info("Writing to %s", filename)
            fig.savefig(filename, bbox_inches="tight", pad_inches=0.5)
            plt.close(fig)
        else:
            plt.show()

    return 0


###################################################################################################
# Body
###################################################################################################


if __name__ == "__main__":
    run_script()

###################################################################################################
# End of file
###################################################################################################
