#!/usr/bin/env python3
# coding: utf-8
###################################################################################################
#  Copyright Jonathan Lucas (c) 2020.
###################################################################################################
#
#    This file is part of cases.
#
#    cases is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    cases is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with cases.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################################

###################################################################################################
# Imports
###################################################################################################

import argparse as ap
import logging
import os
import re
import sys

import matplotlib.pyplot as plt
import numpy as np

from utils import (
    read_world_data,
    filter_const_v,
    add_standard_args,
    remove_outliers,
    read_us_data,
    read_uk_ons_data,
)

plt.rcParams["figure.figsize"] = (8.27, 11.69)

###################################################################################################
# Globals
###################################################################################################

LOG = logging.getLogger(__file__)


def plot_sequence(ax, data, case_type, col_map, plot_cum, args):
    range_alpha = 0.15
    unfilt_alpha = 0.7

    # Plot cumulative values
    if plot_cum:
        ax[1].plot(
            data.X_unfiltered.cumsum(),
            "-.",
            color=col_map["filt"],
            label=f"rep. {case_type} cumulative",
        )

    # Uncomment this to plot rolling data.
    # logplot_data = data.X_rolling.copy()
    # logplot_data[logplot_data <= 0] = np.nan
    #
    # rolling_indicator = ""
    # if args.rolling_mean > 1:
    #     rolling_indicator = f"{args.rolling_mean}-day rolling mean"
    # ax[1].plot(logplot_data,
    #            "x--", color=col_map["unfilt"], alpha=unfilt_alpha,
    #            label=f"reported {case_type} {rolling_indicator}")
    # ax[2].plot(data.X_rolling,
    #            "x--", color=col_map["unfilt"], alpha=unfilt_alpha,
    #            label=f"reported {case_type} {rolling_indicator}")
    #

    # Uncomment this to plot raw data.
    logplot_data = data.X_unfiltered.copy()
    logplot_data[logplot_data <= 0] = np.nan

    rolling_indicator = ""
    ax[1].plot(
        logplot_data,
        "x--",
        color=col_map["unfilt"],
        alpha=unfilt_alpha,
        label=f"reported {case_type} {rolling_indicator}",
    )
    ax[2].plot(
        data.X_unfiltered,
        "x--",
        color=col_map["unfilt"],
        alpha=unfilt_alpha,
        label=f"reported {case_type} {rolling_indicator}",
    )

    # Plot velocities
    ax[0].fill_between(
        data.V.index,
        data.V_upper,
        data.V_lower,
        color=col_map["filt"],
        alpha=range_alpha,
        label="90% confidence limit",
    )
    ax[0].plot(data.V, "-", color=col_map["filt"], label=f"filtered {case_type} estimate")

    # Plot main values
    for axis in ax[1:3]:
        axis.fill_between(
            data.index,
            data.X_upper,
            data.X_lower,
            color=col_map["filt"],
            alpha=range_alpha,
            label="90% confidence limit",
        )
        axis.plot(data.X, "-", color=col_map["filt"], label=f"filtered {case_type} estimate")


def plot_cases(full_data, country, args):
    return_fig = None
    full_colour_map = {
        "cases": {"filt": "tab:blue", "unfilt": "tab:cyan"},
        "deaths": {"filt": "tab:red", "unfilt": "tab:orange"},
        "cases_weekly": {"filt": "tab:blue", "unfilt": "tab:cyan"},
        "deaths_weekly": {"filt": "tab:red", "unfilt": "tab:orange"},
        "admissions": {"filt": "tab:blue", "unfilt": "tab:cyan"},
        "occupancy": {"filt": "tab:green", "unfilt": "tab:olive"},
        "ventilated": {"filt": "tab:red", "unfilt": "tab:orange"},
    }
    plot_cum = {"cases", "deaths", "admissions"}
    fig, ax = plt.subplots(3, 1, sharex="col")
    ax = list(ax)

    axes_map = {
        "cases": ax[0:3],
        "cases_weekly": ax[0:3],
        "admissions": ax[0:3],
        "occupancy": ax[0:3],
        "ventilated": ax[0:3],
    }
    if "deaths" in args.case_type:
        ax.append(ax[2].twinx())  # instantiate a second axes that shares the same x-axis
        axes_map["deaths"] = [ax[0], ax[1], ax[3]]

    if "deaths_weekly" in args.case_type:
        ax.append(ax[2].twinx())  # instantiate a second axes that shares the same x-axis
        axes_map["deaths_weekly"] = [ax[0], ax[1], ax[3]]

    last_date = full_data.index[0].date()
    textstr = []
    for case_type in args.case_type:
        data = full_data[case_type][country]

        if last_date < data.index[-1].date():
            last_date = data.index[-1].date()

        data = data[data >= 0]
        if args.start_at_level:
            data = data[data.cumsum() > args.start_at_level]  # Cut out early stages of outbreak ()

        col_map = full_colour_map[case_type]
        data = data.resample("D").first().dropna()
        data = remove_outliers(data, args)
        if len(data) >= args.est_window or (args.smooth and len(data) > 0):
            try:
                filt = filter_const_v(data, args)

                plot_sequence(axes_map[case_type], filt, case_type, col_map, case_type in plot_cum,
                              args)
                return_fig = fig
                daily_est = filt.V.dropna().iloc[-1]
                weekly_est = (np.exp(filt.log_V.dropna().iloc[-1] * 7) - 1) * 100
                textstr.append(f"current {case_type} (1/7 day rate): {daily_est:+.1f}% / {weekly_est:+.1f}%")
                # textstr.append(f"current {case_type} 7-day est: {weekly_est:+.1f}%")

            except ValueError:
                pass

    if not return_fig:
        plt.close(fig)
    else:
        ax[0].set_title(
            "COVID-19 progression for " f"{country.replace('_', ' ')}: ({', '.join(args.case_type)}) as of {last_date}")
        ax[0].grid()
        ax[0].set_ylabel("%age change per day")
        ax[0].tick_params(axis="x", labelbottom=False)
        textstr = "\n".join(textstr)
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)

        # place a text box in upper left in axes coords
        ax[0].text(0.5, 0.95, textstr, transform=ax[0].transAxes, fontsize=8,
                   verticalalignment='top', bbox=props)

        ax[1].grid()
        ax[1].set_ylabel("incidence (log)")
        ax[1].set_yscale("log")
        ax[1].legend(loc="best", fontsize="xx-small")
        ax[1].set_ylim(1, None)

        ax2_col = full_colour_map[args.case_type[0]]["filt"]
        ax[2].grid(axis="x")
        ax[2].grid(color=ax2_col, axis="y")
        ax[2].set_ylabel(f"{args.case_type[0]} incidence (linear)", color=ax2_col)
        ax[2].tick_params(axis="y", colors=ax2_col)
        ax[2].tick_params(axis="x", rotation=30)

        if "deaths" in args.case_type:
            ax3_col = full_colour_map["deaths"]["filt"]
            ax[3].tick_params(axis="y", colors=ax3_col)
            ax[3].set_ylabel("death incidence (linear)", color=ax3_col)

        if "deaths_weekly" in args.case_type:
            ax3_col = full_colour_map["deaths_weekly"]["filt"]
            ax[3].tick_params(axis="y", colors=ax3_col)
            ax[3].set_ylabel("death incidence (linear)", color=ax3_col)

        plt.tight_layout()

    return return_fig


def run_script():
    parser = ap.ArgumentParser(
        description="Plot UK COVID-19 progress.",
        formatter_class=ap.ArgumentDefaultsHelpFormatter,
    )
    add_standard_args(parser)

    args = parser.parse_args()

    log_levels = [
        logging.CRITICAL,
        logging.ERROR,
        logging.WARNING,
        logging.INFO,
        logging.DEBUG,
        logging.NOTSET,
    ]
    logging.basicConfig(
        format="%(asctime)s.%(msecs)03d %(name)-12s %(levelname)-8s %(message)s",
        datefmt="%m-%d %H:%M:%S",
        # level=log_levels[args.verbosity],  # uncomment for 3rd party msgs
        stream=sys.stderr,
    )
    LOG.setLevel(log_levels[args.verbosity])
    LOG.debug("Command line arguments processed: %s", args)

    if args.input_data.endswith("us-states.csv"):
        data, _, countries = read_us_data(args.input_data, values=args.case_type,
                                          states=args.country)
    elif re.search(r"uk-.*\.csv$", args.input_data):
        data, _, countries = read_uk_ons_data(args.input_data, values=args.case_type,
                                              states=args.country)
    else:
        data, _, countries = read_world_data(args.input_data, values=args.case_type,
                                             countries=args.country)

    # plot_cases(data)
    for country in countries:
        LOG.debug("Processing %s:%s", args.case_type, country)
        fig = plot_cases(data, country, args)
        if fig:
            if args.output:
                filename = os.path.join(args.output,
                                        f"{'_'.join(args.case_type)}_{country}.{args.format}")
                filename = filename.replace(" ", "_")

                LOG.info("Writing to %s", filename)
                fig.savefig(filename, bbox_inches="tight", pad_inches=0.5)
                plt.close(fig)
            else:
                plt.show()

    return 0


###################################################################################################
# Body
###################################################################################################


if __name__ == "__main__":
    run_script()

###################################################################################################
# End of file
###################################################################################################
