#!/usr/bin/env python3
# coding: utf-8
###################################################################################################
#  Copyright Jonathan Lucas (c) 2020.
###################################################################################################
#
#    This file is part of cases.
#
#    cases is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    cases is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with cases.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################################

###################################################################################################
# Imports
###################################################################################################
import sys

from plot_kalman import run_script

COUNTRIES = "UK USA China Japan Germany France Spain Italy"
UK_REGIONS = "London South_West North_West"


###################################################################################################
# Tests
###################################################################################################


def test_01_single_plot(scratch_dir, ecdc_data):
    cmd_line = f"./plot_kalman.py -v3 -i {ecdc_data} -o {scratch_dir} -T cases UK"
    sys.argv = cmd_line.split()
    assert run_script() == 0


def test_02_plot_many(scratch_dir, ecdc_data):
    cmd_line = f"./plot_kalman.py -v3 -i {ecdc_data} -o {scratch_dir} -T cases {COUNTRIES}"
    sys.argv = cmd_line.split()
    assert run_script() == 0


def test_03_single_plot_smoothed(scratch_dir, ecdc_data):
    cmd_line = f"./plot_kalman.py -v3  -S -i {ecdc_data} -o {scratch_dir} -T cases UK"
    sys.argv = cmd_line.split()
    assert run_script() == 0


def test_04_plot_many_smoothed(scratch_dir, ecdc_data):
    cmd_line = f"./plot_kalman.py -v3 -S -i {ecdc_data} -o {scratch_dir} -T cases {COUNTRIES}"
    sys.argv = cmd_line.split()
    assert run_script() == 0


def test_05_plot_multi_case_many_smoothed(scratch_dir, ecdc_data):
    cmd_line = f"./plot_kalman.py -v3 -S -i {ecdc_data} -o {scratch_dir} -f png " f"-T cases -T deaths {COUNTRIES}"
    sys.argv = cmd_line.split()
    assert run_script() == 0


def test_06_plot_florida(scratch_dir, us_data):
    cmd_line = f"./plot_kalman.py -v3 -i {us_data} -o {scratch_dir} -T cases Florida"
    sys.argv = cmd_line.split()
    assert run_script() == 0


def test_07_single_plot_remove_outliers(scratch_dir, ecdc_data):
    cmd_line = f"./plot_kalman.py -v3 -OL 3 -S -i {ecdc_data} -o {scratch_dir} -T cases UK"
    sys.argv = cmd_line.split()
    assert run_script() == 0


def test_08_plot_uk_region(scratch_dir, uk_region_data):
    cmd_line = f"./plot_kalman.py -v3 -i {uk_region_data} -o {scratch_dir} -T cases -T deaths  {UK_REGIONS}"
    sys.argv = cmd_line.split()
    assert run_script() == 0


def test_09_plot_uk_nhs_region(scratch_dir, uk_nhs_region_data):
    cmd_line = f"./plot_kalman.py -v3 -i {uk_nhs_region_data} -o {scratch_dir} " \
               f"-T admissions -T occupancy -T ventilated  {UK_REGIONS}"
    sys.argv = cmd_line.split()
    assert run_script() == 0


###################################################################################################
# End of file
###################################################################################################
