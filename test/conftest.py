#!/usr/bin/env python3
# coding: utf-8
###################################################################################################
#  Copyright Jonathan Lucas (c) 2020.
###################################################################################################
#
#    This file is part of cases.
#
#    cases is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    cases is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with cases.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################################

###################################################################################################
# Imports
###################################################################################################
import os
import shutil

import pytest


###################################################################################################
# Pytest fixtures
###################################################################################################
def local_test_dir() -> str:
    test_name = os.environ["PYTEST_CURRENT_TEST"].split()[0]
    bad_chars = ":$&#@<>"
    mapper = str.maketrans(bad_chars, "_" * len(bad_chars))
    test_name = test_name.translate(mapper)
    test_dir_list = test_name.split("/")
    test_dir = os.path.join(*test_dir_list)
    return test_dir


@pytest.fixture(scope="session")
def common_input_dir():
    return os.path.join("data")


@pytest.fixture(scope="session")
def ecdc_data(common_input_dir):
    return os.path.join(common_input_dir, "world_data.csv")


@pytest.fixture(scope="function")
def scratch_dir():
    dirname = os.path.join("tmp", local_test_dir())
    shutil.rmtree(dirname, ignore_errors=True)
    os.makedirs(dirname, exist_ok=True)
    return dirname


@pytest.fixture(scope="function")
def data_dir():
    dirname = os.path.join("test", "data", local_test_dir())
    return dirname


@pytest.fixture()
def us_data(common_input_dir):
    return os.path.join(common_input_dir, "us-states.csv")


@pytest.fixture()
def uk_region_data(common_input_dir):
    return os.path.join(common_input_dir, "uk-region.csv")


@pytest.fixture()
def uk_nhs_region_data(common_input_dir):
    return os.path.join(common_input_dir, "uk-nhs-regions.csv")


@pytest.fixture()
def uk_nation_data(common_input_dir):
    return os.path.join(common_input_dir, "uk-nation.csv")


@pytest.fixture()
def uk_capacity_data(common_input_dir):
    return os.path.join(common_input_dir, "uk-capacity.csv")

###################################################################################################
# End of file
###################################################################################################
