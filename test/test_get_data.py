#!/usr/bin/env python3
# coding: utf-8
###################################################################################################
#  Copyright Jonathan Lucas (c) 2020.
###################################################################################################
#
#    This file is part of cases.
#
#    cases is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    cases is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with cases.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################################

###################################################################################################
# Imports
###################################################################################################
import os
import sys

from get_data import run_script
import pytest

###################################################################################################
# Tests
###################################################################################################


def test_01_get(scratch_dir, data_dir):
    SOURCE_URL = "https://jigsaw.w3.org/HTTP/TE/foo.txt"
    sys.argv = ["get_data.py", "-u", SOURCE_URL, os.path.join(scratch_dir, "foo.txt")]
    assert run_script() == 0


@pytest.mark.parametrize("area_type", ["nation", "region", "utla", "ltla"])
def test_02_get_ons_uk_gen_regions(scratch_dir, area_type, data_dir):
    sys.argv = ["get_data.py", f"--uk_gen={area_type}", os.path.join(scratch_dir, f"uk-gen-{area_type}.csv")]
    assert run_script() == 0


def test_03_get_ons_uk_nhs_regions(scratch_dir, data_dir):
    sys.argv = ["get_data.py", "--uk_nhs", os.path.join(scratch_dir, "uk-nhs-regions.csv")]
    assert run_script() == 0


def test_04_get_world(scratch_dir, data_dir):
    sys.argv = ["get_data.py", "--j", os.path.join(scratch_dir, "world_data.csv")]
    assert run_script() == 0


###################################################################################################
# End of file
###################################################################################################
