#!/usr/bin/env python3
# coding: utf-8
###################################################################################################
#  Copyright Jonathan Lucas (c) 2020.
###################################################################################################
#
#    This file is part of cases.
#
#    cases is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    cases is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with cases.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################################

###################################################################################################
# Imports
###################################################################################################
import sys

from plot_uk_capacity import run_script

COUNTRIES = "United_Kingdom United_States_of_America China Japan Germany France Spain Italy"
UK_REGIONS = "London South_West North_West"


###################################################################################################
# Tests
###################################################################################################


def test_01_plot_uk_nhs_region(scratch_dir, uk_nhs_region_data, uk_capacity_data):
    cmd_line = f"./plot_uk_capacity.py -v3 -i {uk_nhs_region_data} -o {scratch_dir} " \
               f"-T admissions -T occupancy -T ventilated  {UK_REGIONS}"
    sys.argv = cmd_line.split()
    assert run_script() == 0


###################################################################################################
# End of file
###################################################################################################
