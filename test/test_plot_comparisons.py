#!/usr/bin/env python3
# coding: utf-8
###################################################################################################
#  Copyright Jonathan Lucas (c) 2020.
###################################################################################################
#
#    This file is part of cases.
#
#    cases is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    cases is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with cases.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################################

###################################################################################################
# Imports
###################################################################################################
import sys

from plot_comparisons import run_script

COUNTRIES = "UK USA China Japan Germany France Spain Italy"


###################################################################################################
# Tests
###################################################################################################


def test_01_cases(scratch_dir, ecdc_data):
    cmd_line = f"./plot_comparisons.py -v3 -T cases -i {ecdc_data} -o {scratch_dir} {COUNTRIES}"
    sys.argv = cmd_line.split()
    assert run_script() == 0


def test_02_deaths(scratch_dir, ecdc_data):
    cmd_line = f"./plot_comparisons.py -v3 -T deaths -i {ecdc_data} -o {scratch_dir} {COUNTRIES}"
    sys.argv = cmd_line.split()
    assert run_script() == 0


def test_03_loglog(scratch_dir, ecdc_data):
    cmd_line = f"./plot_comparisons.py -v3 -T cases -P loglog -i {ecdc_data} -o {scratch_dir} {COUNTRIES}"
    sys.argv = cmd_line.split()
    assert run_script() == 0


def test_04_cases_smoothed(scratch_dir, ecdc_data):
    cmd_line = f"./plot_comparisons.py -v3 -T cases -S -i {ecdc_data} -o {scratch_dir} {COUNTRIES}"
    sys.argv = cmd_line.split()
    assert run_script() == 0

###################################################################################################
# End of file
###################################################################################################
