#!/usr/bin/env python3
# coding: utf-8
###################################################################################################
#  Copyright Jonathan Lucas (c) 2020.
###################################################################################################
#
#    This file is part of cases.
#
#    cases is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    cases is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with cases.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################################

###################################################################################################
# Imports
###################################################################################################
import sys

from plot_uk_vaccinations import run_script


###################################################################################################
# Tests
###################################################################################################


def test_01_baseline(scratch_dir, uk_nation_data):
    cmd_line = f"plot_uk_vaccinations.py -i {uk_nation_data} -o {scratch_dir}"
    sys.argv = cmd_line.split()
    assert run_script() == 0


def test_02_1target(scratch_dir, uk_nation_data):
    cmd_line = (
        f"plot_uk_vaccinations.py -i {uk_nation_data} -o {scratch_dir} "
        f"-t 13.9e6 -d 2021-02-15 -C UK"
    )
    sys.argv = cmd_line.split()
    assert run_script() == 0


def test_03_2targets(scratch_dir, uk_nation_data):
    cmd_line = (
        f"plot_uk_vaccinations.py -i {uk_nation_data} -o {scratch_dir} "
        f"-t 13.9e6 32e6 -d 2021-02-15 2021-05-01 -C UK"
    )
    sys.argv = cmd_line.split()
    assert run_script() == 0


def test_04_3targets(scratch_dir, uk_nation_data):
    cmd_line = (
        f"plot_uk_vaccinations.py -i {uk_nation_data} -o {scratch_dir} "
        f"-t 13.9e6 32e6 52.9e6 -d 2021-02-15 2021-05-01 2021-07-31 -C UK"
    )
    sys.argv = cmd_line.split()
    assert run_script() == 0


def test_05_3targets_combined(scratch_dir, uk_nation_data):
    cmd_line = (
        f"plot_uk_vaccinations.py -i {uk_nation_data} -o {scratch_dir} "
        f"-t 13.9e6 32e6 52.9e6 -d 2021-02-15 2021-05-01 2021-07-31 -C UK -c"
    )
    sys.argv = cmd_line.split()
    assert run_script() == 0

###################################################################################################
# End of file
###################################################################################################
