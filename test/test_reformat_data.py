#!/usr/bin/env python3
# coding: utf-8
###################################################################################################
#  Copyright Jonathan Lucas (c) 2020.
###################################################################################################
#
#    This file is part of cases.
#
#    cases is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    cases is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with cases.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################################

###################################################################################################
# Imports
###################################################################################################
import os
import sys

from reformat_data import run_script


###################################################################################################
# Tests
###################################################################################################


def test_01_csv(scratch_dir, data_dir):
    sys.argv = ["reformat_data.py", "-o", os.path.join(scratch_dir, "world_%s.csv")]
    assert run_script() == 0


def test_02_xlsx(scratch_dir, data_dir):
    sys.argv = ["reformat_data.py", "-o", os.path.join(scratch_dir, "world_%s.xlsx")]
    assert run_script() == 0

###################################################################################################
# End of file
###################################################################################################
