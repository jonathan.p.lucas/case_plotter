#!/usr/bin/env bash
# coding: utf-8
###################################################################################################
#  Copyright Jonathan Lucas (c) 2020.
###################################################################################################
#
#    This file is part of cases.
#
#    cases is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    cases is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with cases.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################################
set -e

COUNTRIES="UK USA France Germany Italy Spain China Japan Netherlands Belgium"
US_STATES="Florida California"
UK_REGIONS="London South_West"
UK_NHS_REGIONS="London South_West"

rm -rf examples/*

echo Generating interim data...
./reformat_data.py -o data/world_%s.csv
./reformat_data.py -o data/world_%s.xlsx

echo Generating UK hospitalisation plots ...
./plot_uk_hospitalisation.py -S -o examples
./plot_uk_capacity.py -v3 -R7 -m0.00005 -i data/uk-nhs-regions.csv -T admissions -T occupancy -T ventilated -f png -S -o examples $UK_NHS_REGIONS

echo Generating UK vaccination plot
./plot_uk_vaccinations.py -v3 -o examples -i data/uk-nation.csv -f png

echo Generating comparison plots ...
./plot_comparisons.py -v3 -i data/world_data.csv -f png -o examples $COUNTRIES
./plot_comparisons.py -v3 -i data/world_data.csv -S -T cases -P loglog -f png -o examples  $COUNTRIES

echo Generating Kalman plots ...
./plot_kalman.py -v3 -i data/world_data.csv -S -T cases -T deaths -f png -o examples $COUNTRIES
./plot_kalman.py -v3 -i data/us-states.csv -T cases -T deaths -f png -S -o examples $US_STATES
./plot_kalman.py -v3 -i data/uk-regions.csv -T deaths -T cases -f svg -o examples $UK_REGIONS

###################################################################################################
# End of file
###################################################################################################
