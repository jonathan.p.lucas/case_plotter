#!/usr/bin/env python3
# coding: utf-8
###################################################################################################
#  Copyright Jonathan Lucas (c) 2020.
###################################################################################################
#
#    This file is part of cases.
#
#    cases is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    cases is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with cases.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################################

###################################################################################################
# Imports
###################################################################################################

import argparse as ap
import logging
import os
import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from utils import (
    filter_const_v,
    add_standard_args,
    remove_outliers,
    read_uk_ons_data,
)

plt.rcParams["figure.figsize"] = (8.27, 11.69)

###################################################################################################
# Globals
###################################################################################################

LOG = logging.getLogger(__file__)


def plot_sequence(ax, data, case_type, col_map, args):
    range_alpha = 0.15
    unfilt_alpha = 0.7

    # Uncomment this to plot raw data.
    logplot_data = data.X_unfiltered.copy()
    logplot_data[logplot_data <= 0] = np.nan

    ax[1].plot(
        logplot_data,
        "x--",
        color=col_map["unfilt"],
        alpha=unfilt_alpha,
        label=f"{case_type}",
    )
    # Plot main values
    ax[1].fill_between(
        data.index,
        data.X_upper,
        data.X_lower,
        color=col_map["filt"],
        alpha=range_alpha,
        label="90% confidence limit",
    )
    ax[1].plot(data.X, "-", color=col_map["filt"], label=f"filtered {case_type} estimate")

    # Plot velocities
    ax[0].fill_between(
        data.V.index,
        data.V_upper,
        data.V_lower,
        color=col_map["filt"],
        alpha=range_alpha,
        label="90% confidence limit",
    )
    ax[0].plot(data.V, "-", color=col_map["filt"], label=f"filtered {case_type} estimate")


def plot_cases(full_data, country, capacity, args):
    # CMAP = [plt.get_cmap("tab20").colors[0::2], plt.get_cmap("tab20").colors[1::2]]
    # full_colour_map = {
    #     "cases": {"filt": CMAP[1][0], "unfilt": CMAP[0][0]},
    #     "deaths": {"filt": CMAP[1][1], "unfilt": CMAP[0][1]}
    # }
    return_fig = None
    full_colour_map = {
        "admissions": {"filt": "tab:blue", "unfilt": "tab:cyan"},
        "occupancy": {"filt": "tab:green", "unfilt": "tab:olive"},
        "ventilated": {"filt": "tab:red", "unfilt": "tab:orange"},
    }
    fig, ax = plt.subplots(2, 1, sharex="col")
    ax = list(ax)

    textstr = []
    for case_type in args.case_type:
        data = full_data[case_type][country]
        data = data[data >= 0]
        if args.start_at_level:
            data = data[data.cumsum() > args.start_at_level]  # Cut out early stages of outbreak ()

        col_map = full_colour_map[case_type]
        data = data.resample("D").first().dropna()
        data = remove_outliers(data, args)
        if len(data) >= args.est_window or (args.smooth and len(data) > 0):
            try:
                filt = filter_const_v(data, args)

                plot_sequence(ax, filt, case_type, col_map, args)
                return_fig = fig
                daily_est = filt.V.dropna().iloc[-1]
                weekly_est = (np.exp(filt.log_V.dropna().iloc[-1] * 7) - 1) * 100
                textstr.append(f"current {case_type} (1/7 day rate): {daily_est:+.1f}% / {weekly_est:+.1f}%")

            except ValueError:
                pass

    if capacity:
        ax[1].axhline(capacity, color="magenta", label="General and acute bed capacity")

    if not return_fig:
        plt.close(fig)
    else:
        last_date = data.index[-1].date()
        ax[0].set_title(
            f"COVID-19 hospital status for {country.replace('_', ' ')} as of {last_date}")
        textstr = "\n".join(textstr)
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)

        # place a text box in upper left in axes coords
        ax[0].text(0.5, 0.95, textstr, transform=ax[0].transAxes, fontsize=8,
                   verticalalignment='top', bbox=props)

        ax[0].grid()
        ax[0].set_ylabel("%age change per day")
        ax[0].tick_params(axis="x", labelbottom=False)

        ax[1].grid()
        ax[1].set_ylabel("incidence (log)")
        ax[1].set_yscale("log")
        ax[1].set_ylim(1, None)
        ax[1].legend(loc="best", fontsize="xx-small")

        plt.tight_layout()

    return return_fig


def run_script():
    parser = ap.ArgumentParser(
        description="Plot UK COVID-19 progress.",
        formatter_class=ap.ArgumentDefaultsHelpFormatter,
    )
    add_standard_args(parser)

    args = parser.parse_args()

    log_levels = [
        logging.CRITICAL,
        logging.ERROR,
        logging.WARNING,
        logging.INFO,
        logging.DEBUG,
        logging.NOTSET,
    ]
    logging.basicConfig(
        format="%(asctime)s.%(msecs)03d %(name)-12s %(levelname)-8s %(message)s",
        datefmt="%m-%d %H:%M:%S",
        # level=log_levels[args.verbosity],  # uncomment for 3rd party msgs
        stream=sys.stderr,
    )
    LOG.setLevel(log_levels[args.verbosity])
    LOG.debug("Command line arguments processed: %s", args)

    data, _, regions = read_uk_ons_data(args.input_data, values=args.case_type,
                                        states=args.country)

    capacity = pd.read_csv(args.capacity_data, index_col="region")["capacity"]

    sum = data.sum(axis=1, level=0, skipna=False)
    sum.columns = pd.MultiIndex.from_product([sum.columns, ["England"]])
    data = pd.concat([data, sum], axis=1)

    regions.append("England")
    capacity["England"] = capacity.sum()
    # plot_cases(data)
    for region in regions:
        LOG.debug("Processing %s:%s", args.case_type, region)
        fig = plot_cases(data, region, capacity[region], args)
        if fig:
            if args.output:
                filename = os.path.join(args.output,
                                        f"{'_'.join(args.case_type)}_{region}.{args.format}")

                LOG.info("Writing to %s", filename)
                fig.savefig(filename, bbox_inches="tight", pad_inches=0.5)
                plt.close(fig)
            else:
                plt.show()

    return 0


###################################################################################################
# Body
###################################################################################################


if __name__ == "__main__":
    run_script()

###################################################################################################
# End of file
###################################################################################################
