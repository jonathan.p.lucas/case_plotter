#!/usr/bin/env python3
# coding: utf-8
###################################################################################################
#  Copyright Jonathan Lucas (c) 2020.
###################################################################################################
#
#    This file is part of cases.
#
#    cases is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    cases is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with cases.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################################

###################################################################################################
# Imports
###################################################################################################
import pandas as pd

###################################################################################################
# Globals
###################################################################################################

POPULATION = pd.Series(
    {
        "England": 56286961,
        "Northern_Ireland": 1893667,
        "Scotland": 5463300,
        "Wales": 3152879
    }
)

POPULATION["UK"] = POPULATION.sum()

###################################################################################################
# Body
###################################################################################################

###################################################################################################
# End of file
###################################################################################################
