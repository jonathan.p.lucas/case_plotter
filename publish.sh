#!/usr/bin/env bash
# coding: utf-8
###################################################################################################
#  Copyright Jonathan Lucas (c) 2020.
###################################################################################################
#
#    This file is part of cases.
#
#    cases is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    cases is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with cases.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################################
set -e

COUNTRY_LIST="UK USA France Germany Spain Japan Belgium Sweden Russia India Brazil Mexico"
REDUCED_COUNTRY_LIST="UK USA Germany Japan Belgium Sweden India Brazil Mexico"
#FORMATS="png pdf svg jpg"
FORMATS="png"
rm -rf public
mkdir -p public/US
mkdir -p public/UK
./update_data.sh

./plot_uk_hospitalisation.py -S -o public/UK &
./plot_uk_hosp_comp.py -S -o public/UK &
./plot_uk_vaccinations.py -o public/UK -i data/uk-nation.csv -f png -t 0 15e6 32e6 52e6 -d 2020-12-08 2021-02-15 2021-04-15 2021-07-31 -t2o 84 -C UK &
./plot_uk_vaccinations.py -o public/UK -i data/uk-nation.csv -f png -t 0 15e6 32e6 52e6 -d 2020-12-08 2021-02-15 2021-04-15 2021-07-31 -t2o 84 -C UK -c
for FORMAT in ${FORMATS}
do
  echo Generating ${FORMAT} comparison plots ...
  ./plot_comparisons.py -i data/world_data.csv -l2 -P loglog -S -T deaths -f ${FORMAT} -o public ${COUNTRY_LIST} &
  ./plot_comparisons.py -i data/world_data.csv -l500 -P loglog -S -T cases -f ${FORMAT} -o public ${COUNTRY_LIST} &

  ./plot_comparisons.py -i data/world_data.csv -l1e-7 -P timeslip -S -T deaths -f ${FORMAT} -o public ${REDUCED_COUNTRY_LIST} &
  ./plot_comparisons.py -i data/world_data.csv -l1e-6 -P timeslip -S -T cases -f ${FORMAT} -o public ${REDUCED_COUNTRY_LIST} &
done
wait
for FORMAT in ${FORMATS}
do
  echo Generating joint kalman plots...
  ./plot_kalman.py -v3 -i data/world_data.csv -T cases -T deaths -f ${FORMAT} -S -o public &
  ./plot_kalman.py -v3 -i data/us-states.csv -T cases -T deaths -f ${FORMAT} -S -o public/US &
  ./plot_kalman.py -v3 -R7 -m0.0001 -i data/uk-overview.csv -T cases -f ${FORMAT} -S -o public/UK &
  ./plot_kalman.py -v3 -R7 -m0.0001 -i data/uk-region.csv -T cases -T deaths -f ${FORMAT} -S -o public/UK &
  ./plot_kalman.py -v3 -R7 -m0.0001 -i data/uk-nation.csv -T cases -T deaths -f ${FORMAT} -S -o public/UK -OL 0 &
  ./plot_kalman.py -v3 -R7 -m0.0001 -i data/uk-utla.csv -T cases -T deaths -f ${FORMAT} -S -o public/UK &
  ./plot_uk_capacity.py -v3 -R3 -m0.000075 -i data/uk-nhs-regions.csv -T admissions -T occupancy -T ventilated -f ${FORMAT} -S -o public/UK &
done
for TYPE in cases deaths
do
  for FORMAT in ${FORMATS}
  do
    echo Generating ${FORMAT} kalman plots for ${TYPE}...
    ./plot_kalman.py -v3 -R7 -m0.00005 -i data/uk-region.csv -T ${TYPE} -f ${FORMAT} -S -o public/UK &
    ./plot_kalman.py -v3 -R7 -m0.00005 -i data/uk-nation.csv -T ${TYPE} -f ${FORMAT} -S -o public/UK -OL 0 &
    ./plot_kalman.py -v3 -R7 -m0.0001 -i data/uk-utla.csv -T ${TYPE} -f ${FORMAT} -S -o public/UK &
  done
done
wait


cp -rv data public/

###################################################################################################
# End of file
###################################################################################################
