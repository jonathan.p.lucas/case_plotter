[![pipeline status](https://gitlab.com/jonathan.p.lucas/case_plotter/badges/master/pipeline.svg)](https://gitlab.com/jonathan.p.lucas/case_plotter/-/commits/master)
[![coverage report](https://gitlab.com/jonathan.p.lucas/case_plotter/badges/master/coverage.svg)](https://gitlab.com/jonathan.p.lucas/case_plotter/-/commits/master)

# Worldwide COVID19 case/deaths plotter using Kalman filters

This project provides a number of utilities which aim to track the progress of a country in its 
attempt to slow down or halt the progress of COVID19. 

Specifically it exploits the short-term 
exponential nature of the infection curve by modelling the process as a log-domain 
constant-velocity process in a Kalman filter or smoother. This allows us to estimate the mean 
and covariance level of current infection level and its daily rate of change as latent variables 
of the Kalman filter, giving us a filtered view with error estimates. The result is something 
like this:

## plot_kalman.py

### Online filter update

```bash
./plot_kalman.py -T cases -T deaths UK -S -i data/world_data.csv -f png -o public 
```

Produces plots like this one, generated using a Kalman smoother:

![Sample Kalman Plot](examples/cases_deaths_UK.png "plot_kalman.py for United Kingdom")

### Online filter update

```bash
./plot_kalman.py -i data/uk-regions.csv -T deaths -T cases -f svg -o examples London
```

Produces plots like this one, generated using an online kalman filter update - more noisy, but
makes continual online estimates of measurement noisiness based on residuals on a linear fit 
(in the log domain) of the data over a fixed window around the current measurement:

![Sample Kalman Plot](examples/deaths_cases_London.svg "plot_kalman.py for London")

## plot_comparisons.py

The command line

```bash
./plot_comparisons.py -i data/world_data.csv -l50 -S -P loglog -T cases -f png -o public UK USA France Germany Italy Spain China Japan Netherlands Belgium
```

Produces plots like this one:

![Sample Kalman Comparison Plot](examples/cases_loglog_comparison.png "plot_comparisons.py for United Kingdom")