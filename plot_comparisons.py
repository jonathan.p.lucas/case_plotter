#!/usr/bin/env python3
# coding: utf-8
###################################################################################################
#  Copyright Jonathan Lucas (c) 2020.
###################################################################################################
#
#    This file is part of cases.
#
#    cases is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    cases is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with cases.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################################

###################################################################################################
# Imports
###################################################################################################

import argparse as ap
import logging
import os
import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from utils import read_world_data, filter_const_v, add_standard_args, \
    generate_colour_marker_lookup

plt.rcParams["figure.figsize"] = (11.69, 8.27)

###################################################################################################
# Globals
###################################################################################################

LOG = logging.getLogger(__file__)


def rate(data):
    elapsed = data.index
    ylog = np.log(data)
    fit = np.polyfit(elapsed, ylog, deg=1)
    percentage_change = (np.exp(fit[0]) - 1) * 100
    return percentage_change


def plot_comparisons(country_data, population_inf, case_type, args):
    country_cols = generate_colour_marker_lookup(country_data.columns)

    fig, ax = plt.subplots(2, 1, sharex=True)
    ax[0].set_title(f"COVID-19 {case_type} progression by country")
    for country in country_data:
        data = pd.DataFrame({"X": country_data[country]})
        data = data[data.X.cumsum() / population_inf[country] > args.start_at_level]
        data["days_since_start"] = range(len(data))
        data = data.resample("D").first().dropna()
        data["unfilt_cumsum"] = data.X.cumsum()

        if len(data) >= args.est_window or (args.smooth and len(data) > 0):
            data["kalman_X"] = filter_const_v(data.X, args).X
            data.index = data.days_since_start
            ax[0].plot(data.kalman_X / population_inf[country],
                       "-", **country_cols[country],
                       label=country, alpha=0.7)
            ax[1].plot(data.kalman_X.cumsum() / population_inf[country],
                       "-", **country_cols[country],
                       label=country, alpha=0.7)

    ax[0].set_ylabel(f"{case_type} per capita ")
    ax[0].tick_params(axis='x', labelbottom=False)
    ax[0].set_yscale("log")
    ax[0].set_ylim(args.start_at_level / 1e1, None)
    ax[0].grid()

    ax[1].set_ylabel(f"cumulative {case_type} per capita")
    # ax[1].tick_params(axis='x', labelbottom=False)
    ax[1].set_yscale("log")
    ax[1].grid()

    ax[-1].legend(loc="best")
    ax[-1].set_xlabel("Days since outbreak")
    plt.tight_layout()
    if args.output:
        filename = os.path.join(args.output,
                                f"{case_type}_{args.plot_type}_comparison.{args.format}")

        LOG.info("Writing to %s", filename)
        plt.savefig(filename, bbox_inches='tight', pad_inches=0.5, pil_kwargs={"quality": 80})
        plt.close(fig)

    return fig


def plot_loglog_comparison(country_data, population_inf, case_type, args):
    country_cols = generate_colour_marker_lookup(country_data.columns)

    fig, ax = plt.subplots(1, 1, sharex=True)
    for country in country_data:
        data = pd.DataFrame({"X": country_data[country]})
        data = data[data.X.cumsum() > args.start_at_level]
        data = data.resample("D").first().dropna()

        data["unfilt_cumsum"] = data.X.cumsum()

        if len(data) >= args.est_window or (args.smooth and len(data) > 0):
            data["kalman_X"] = filter_const_v(data.X, args).X
            data = data / population_inf[country]
            ax.plot(data.kalman_X.cumsum(), data.kalman_X,
                    "-", **country_cols[country],
                    label=country, alpha=0.7)

    ax.set_title(f"COVID-19 {case_type} progression")
    ax.set_yscale("log")
    ax.set_xscale("log")
    ax.set_ylabel(f"Daily {case_type} per capita")
    ax.set_xlabel(f"Cumulative {case_type} per capita")
    ax.set_xlim(1e-7, None)
    ax.set_ylim(1e-9, None)
    ax.grid()
    ax.legend(loc="best")

    plt.tight_layout()
    if args.output:
        filename = os.path.join(args.output,
                                f"{case_type}_{args.plot_type}_comparison.{args.format}")
        plt.savefig(filename, bbox_inches='tight', pad_inches=0.5, pil_kwargs={"quality": 80})
        plt.close(fig)
    return fig


def run_script():
    parser = ap.ArgumentParser(description="Plot UK COVID-19 progress.",
                               formatter_class=ap.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-P", "--plot_type", action='store', default="timeslip",
                        choices=["loglog", "timeslip"],
                        help="Destination format.")
    add_standard_args(parser)

    args = parser.parse_args()

    log_levels = [
        logging.CRITICAL,
        logging.ERROR,
        logging.WARNING,
        logging.INFO,
        logging.DEBUG,
        logging.NOTSET
    ]
    logging.basicConfig(format='%(asctime)s.%(msecs)03d %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%m-%d %H:%M:%S',
                        # level=log_levels[args.verbosity],  # uncomment for 3rd party msgs
                        stream=sys.stderr)
    LOG.setLevel(log_levels[args.verbosity])
    LOG.debug("Command line arguments processed: %s", args)

    data, pop_data, _ = read_world_data(args.input_data, values=args.case_type, countries=args.country)
    for case_type in args.case_type:
        case_data = data[case_type]
        if args.plot_type == "timeslip":
            plot_comparisons(case_data, pop_data, case_type, args)
        else:  # args.plot_type=="loglog":
            plot_loglog_comparison(case_data, pop_data, case_type, args)

    if not args.output:
        plt.show()

    return 0


###################################################################################################
# Body
###################################################################################################


if __name__ == "__main__":
    run_script()

###################################################################################################
# End of file
###################################################################################################
