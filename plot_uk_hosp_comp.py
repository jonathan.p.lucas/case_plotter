#!/usr/bin/env python3
# coding: utf-8
###################################################################################################
#  Copyright Jonathan Lucas (c) 2020.
###################################################################################################
#
#    This file is part of cases.
#
#    cases is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    cases is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with cases.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################################

###################################################################################################
# Imports
###################################################################################################

import argparse as ap
import logging
import os
import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pykalman import KalmanFilter

from utils import add_standard_args, remove_outliers, \
    read_uk_hospital_data, generate_colour_marker_lookup

plt.rcParams["figure.figsize"] = (8.27, 11.69)

###################################################################################################
# Globals
###################################################################################################

LOG = logging.getLogger(__file__)


def filter(unfiltered, args):
    """
    Perform Kalman filter on data in log domain.
    :param series: Input data (assumed to be in the linear domain)
    :param est_window: Lookback window used to estimate measurement noise.
    :param model_cov_weight: transition matrix noise hyperparameter. Make this bigger to
        reduce filtering effect
    :return: Dataframe containing filtered data.
    """
    series = unfiltered.dropna()

    transition_matrix = np.array([
        [1, 1, 1],
        [0, 1, 1],
        [0, 0, 1]])
    trans_cov = args.model_cov_weight * np.array([
        [1 / 20, 1 / 8, 1 / 6],
        [1 / 8, 1 / 3, 1 / 2],
        [1 / 6, 1 / 2, 1 / 1]])

    # Very rough estimate of initial velocity and accel.
    init_state = [0, unfiltered[0], unfiltered[1] - 2 * unfiltered[0]]

    kf = KalmanFilter(initial_state_mean=init_state, transition_matrices=transition_matrix,
                      transition_covariance=trans_cov)
    means, covars = kf.smooth(series)
    X = means[:, 0]
    X_var = covars[:, 0, 0]
    V = means[:, 1]
    V_var = covars[:, 1, 1]
    A = means[:, 2]
    A_var = covars[:, 2, 2]

    result = pd.DataFrame({
        "X_unfiltered": unfiltered,
        "X": pd.Series(X, index=series.index),
        "X_var": pd.Series(X_var, index=series.index),
        "V": pd.Series(V, index=series.index),
        "V_var": pd.Series(V_var, index=series.index),
        "A": pd.Series(A, index=series.index),
        "A_var": pd.Series(A_var, index=series.index),
    })

    result.V = result.V / result.X * 100.
    result.V_var = result.V_var / (result.X * 100) ** 2.

    result["X_upper"] = result.X + result.X_var ** 0.5 * 2
    result["X_lower"] = result.X - result.X_var ** 0.5 * 2

    result["V_upper"] = result.V + result.V_var ** 0.5 * 2
    result["V_lower"] = result.V - result.V_var ** 0.5 * 2

    return result


def plot_sequence(ax, data, country, col_map):
    alpha = 0.7
    ax[0].plot(data.V,
               color=col_map["color"], marker=col_map["marker"], alpha=alpha,
               label=f"{country}")

    ax[1].plot(data.X,
               color=col_map["color"], marker=col_map["marker"], alpha=alpha,
               label=f"{country}")


def plot_cases(all_data, args):
    countries = args.country
    if len(countries) == 0:
        countries = all_data.columns

    countries = [c for c in countries if not c.startswith("Unnamed")]
    fig, ax = plt.subplots(2, 1, sharex="col")
    full_colour_map = generate_colour_marker_lookup(countries)
    for country in countries:
        data = all_data[country]
        data.dropna(inplace=True)

        if args.start_at_level:
            data = data[data.cumsum() > args.start_at_level]  # Cut out early stages of outbreak ()

        col_map = full_colour_map[country]
        data = data.resample("D").first().dropna()
        data = remove_outliers(data, args)
        if len(data) >= args.est_window or (args.smooth and len(data) > 0):
            filt = filter(data, args)

            plot_sequence(ax, filt.iloc[0:], country, col_map)

    ax[0].set_title("UK COVID-19 hospital cases by region")
    ax[0].grid()
    ax[0].set_ylim(-5, 10)
    ax[0].set_ylabel("%age change per day")
    ax[0].tick_params(axis='x', labelbottom=False)
    ax[0].legend(loc="best")

    ax[1].grid()
    ax[1].set_ylabel("case inncidence (linear)")
    ax[1].tick_params(axis='x', rotation=30)
    ax[1].legend(loc="best")

    plt.tight_layout()

    return fig


def run_script():
    parser = ap.ArgumentParser(description="Plot UK COVID-19 progress.",
                               formatter_class=ap.ArgumentDefaultsHelpFormatter)
    add_standard_args(parser)

    args = parser.parse_args()

    log_levels = [
        logging.CRITICAL,
        logging.ERROR,
        logging.WARNING,
        logging.INFO,
        logging.DEBUG,
        logging.NOTSET
    ]
    logging.basicConfig(format='%(asctime)s.%(msecs)03d %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%m-%d %H:%M:%S',
                        # level=log_levels[args.verbosity],  # uncomment for 3rd party msgs
                        stream=sys.stderr)
    LOG.setLevel(log_levels[args.verbosity])
    LOG.debug("Command line arguments processed: %s", args)

    data = read_uk_hospital_data("data/UK_COVID-19_Press_Conference_Data.xlsx")
    fig = plot_cases(data, args)
    if fig:
        if args.output:
            filename = os.path.join(args.output, "UK_hospitalisation_comparison.png")
            filename = filename.replace(" ", "_")

            LOG.info("Writing to %s", filename)
            fig.savefig(filename, bbox_inches='tight', pad_inches=0.5, pil_kwargs={"quality": 80})
            plt.close(fig)
        else:
            plt.show()

    return 0


###################################################################################################
# Body
###################################################################################################


if __name__ == "__main__":
    run_script()

###################################################################################################
# End of file
###################################################################################################
