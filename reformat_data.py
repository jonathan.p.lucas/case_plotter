#!/usr/bin/env python3
# coding: utf-8
###################################################################################################
#  Copyright Jonathan Lucas (c) 2020.
###################################################################################################
#
#    This file is part of cases.
#
#    cases is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    cases is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with cases.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################################

###################################################################################################
# Imports
###################################################################################################


import argparse as ap
import logging
import sys

from utils import read_world_data

###################################################################################################
# Globals
###################################################################################################

LOG = logging.getLogger(__file__)


def run_script():
    parser = ap.ArgumentParser(
        description="Rework ECDC data into form moreuseful for general analysis."
                    "Supports .csv and .xlsx output formats",
        formatter_class=ap.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-v", "--verbosity", type=int, choices=(0, 1, 2, 3, 4), default=2,
        help="Set verbosity level for debug (0=silent, 4=maximum chattiness)")
    parser.add_argument(
        "-i", "--input", type=str, default="data/world_data.csv", help="Source filename.")
    parser.add_argument(
        "-o", "--output", type=str, default="data/world_cases.csv", help="Destination filename.")
    parser.add_argument("country", nargs="*", help="Plot this country.")

    args = parser.parse_args()

    log_levels = [
        logging.CRITICAL,
        logging.ERROR,
        logging.WARNING,
        logging.INFO,
        logging.DEBUG,
        logging.NOTSET
    ]
    logging.basicConfig(format='%(asctime)s.%(msecs)03d %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%m-%d %H:%M:%S',
                        # level=log_levels[args.verbosity],  # uncomment for 3rd party msgs
                        stream=sys.stderr)
    LOG.setLevel(log_levels[args.verbosity])
    LOG.debug("Command line arguments processed: %s", args)

    data, _, _ = read_world_data(args.input, countries=args.country)
    data.index = data.index.strftime("%Y-%m-%d")
    for case_type in data.columns.get_level_values(0).unique():
        output_fname = args.output % case_type
        if output_fname.endswith(".csv"):
            data[case_type].to_csv(output_fname, index=True, index_label="Date")
        elif output_fname.endswith(".xlsx"):
            data[case_type].to_excel(output_fname, index=True, index_label="Date")
        else:
            raise NotImplementedError("Unsupported file extension")
    return 0


###################################################################################################
# Tests
###################################################################################################

###################################################################################################
# Body
###################################################################################################


if __name__ == "__main__":
    run_script()

###################################################################################################
# End of file
###################################################################################################
