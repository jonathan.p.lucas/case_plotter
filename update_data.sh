#!/usr/bin/env bash
# coding: utf-8
###################################################################################################
#  Copyright Jonathan Lucas (c) 2020.
###################################################################################################
#
#    This file is part of cases.
#
#    cases is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    cases is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with cases.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################################
set -e

CURDATE=${1:-`date +%Y-%m-%d`}
echo Retrieving data for ${CURDATE} ...

echo world_data_weekly
./get_data.py -u https://opendata.ecdc.europa.eu/covid19/casedistribution/csv/data.csv data/world_data_weekly.csv &
./get_data.py -u https://github.com/nytimes/covid-19-data/raw/master/us-states.csv data/us-states.csv &
./get_data.py -j data/world_data.csv &
./get_data.py --uk_nhs data/uk-nhs-regions.csv &
./get_data.py -u https://covid.ourworldindata.org/data/owid-covid-data.csv data/owid-covid-data.csv &
./get_data.py -u "https://api.coronavirus.data.gov.uk/v2/data?areaType=nation&metric=newCasesBySpecimenDateAgeDemographics&metric=newDeaths28DaysByDeathDateAgeDemographics&format=csv" data/uk_nation_cases_deaths_by_age.csv &

for area_type in overview nation region utla ltla
do
  echo "Retrieving UK ${area_type} data"
  ./get_data.py --uk_gen=$area_type data/uk-${area_type}.csv &
done
wait

echo Generating interim data...
./reformat_data.py -i data/world_data.csv -o data/world_%s.csv &
./reformat_data.py -i data/world_data.csv -o data/world_%s.xlsx &
wait

###################################################################################################
# End of file
###################################################################################################
