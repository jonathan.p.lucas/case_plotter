#!/usr/bin/env python3
# coding: utf-8
###################################################################################################
#  Copyright Jonathan Lucas (c) 2020.
###################################################################################################
#
#    This file is part of cases.
#
#    cases is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    cases is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with cases.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################################

###################################################################################################
# Imports
###################################################################################################

import argparse as ap
import logging
import sys
from io import StringIO
from subprocess import run  # noqa

import pandas as pd
import requests
from uk_covid19 import Cov19API

###################################################################################################
# Globals
###################################################################################################

LOG = logging.getLogger(__file__)


def run_script():
    parser = ap.ArgumentParser(
        description="Download latest COVID-19 case data.",
        formatter_class=ap.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--uk_gen",
        action="store",
        type=str,
        choices=["overview", "region", "nation", "utla", "ltla"],
        help="Use UK ONS API for general area data",
    )
    parser.add_argument(
        "-j",
        "--jhucsse",
        action="store_true",
        help="Use https://disease.sh/ Johns Hopkins world data",
    )
    parser.add_argument(
        "--uk_nhs", action="store_true", help="Use UK ONS API for NHS area data"
    )
    parser.add_argument(
        "-v",
        "--verbosity",
        type=int,
        choices=(0, 1, 2, 3, 4),
        default=2,
        help="Set verbosity level for debug (0=silent, 4=maximum chattiness)",
    )
    parser.add_argument(
        "-u",
        "--url",
        type=str,
        nargs="?",
        default="https://www.arcgis.com/sharing/rest/content/items/e5fd11150d274bebaaf8fe2a7a2bda11/data",
        # noqa
        help="Source URL.",
    )
    parser.add_argument(
        "output",
        nargs="?",
        type=str,
        default="data/uk_data.xlsx",
        help="Destination to write to.",
    )

    args = parser.parse_args()

    log_levels = [
        logging.CRITICAL,
        logging.ERROR,
        logging.WARNING,
        logging.INFO,
        logging.DEBUG,
        logging.NOTSET,
    ]
    logging.basicConfig(
        format="%(asctime)s.%(msecs)03d %(name)-12s %(levelname)-8s %(message)s",
        datefmt="%m-%d %H:%M:%S",
        # level=log_levels[args.verbosity],  # uncomment for 3rd party msgs
        stream=sys.stderr,
    )
    LOG.setLevel(log_levels[args.verbosity])
    LOG.debug("Command line arguments processed: %s", args)

    local_name = args.output
    LOG.info("Retrieving new copy of %s", local_name)
    if args.uk_gen:
        gen_data = Cov19API(
            filters=[f"areaType={args.uk_gen}"],
            structure={
                "date": "date",
                "region": "areaName",
                "cases": "newCasesByPublishDate",
                "deaths": "newDeathsByDeathDate",
                "weeklyVaccineFirstDose": "weeklyPeopleVaccinatedFirstDoseByVaccinationDate",
                "weeklyVaccineSecondDose": "weeklyPeopleVaccinatedSecondDoseByVaccinationDate",
                "weeklyCumVaccinatedFirstDose": "cumPeopleVaccinatedFirstDoseByVaccinationDate",
                "weeklyCumVaccinatedSecondDose": "cumPeopleVaccinatedSecondDoseByVaccinationDate",
                "dailyCumVaccinatedFirstDose": "cumPeopleVaccinatedFirstDoseByPublishDate",
                "dailyCumVaccinatedSecondDose": "cumPeopleVaccinatedSecondDoseByPublishDate",
            },
        ).get_csv()
        gen_data = StringIO(gen_data)
        gen_data = pd.read_csv(gen_data)
        gen_data.region = gen_data.region.apply(lambda x: x.replace(" ", "_"))
        gen_data.region = gen_data.region.apply(lambda x: x.replace(",", "_"))
        gen_data.to_csv(args.output, index=False)
    elif args.uk_nhs:
        nhs_data = Cov19API(
            filters=["areaType=nhsRegion"],
            structure={
                "date": "date",
                "region": "areaName",
                "admissions": "newAdmissions",
                "ventilated": "covidOccupiedMVBeds",
                "occupancy": "hospitalCases",
            },
        ).get_csv()
        nhs_data = StringIO(nhs_data)
        nhs_data = pd.read_csv(nhs_data)
        nhs_data.region = nhs_data.region.apply(lambda x: x.replace(" ", "_"))
        nhs_data.region = nhs_data.region.apply(lambda x: x.replace(",", "_"))
        nhs_data.to_csv(args.output, index=False)
    elif args.jhucsse:
        url = "https://disease.sh/v3/covid-19/historical?lastdays=all"
        # url = "https://disease.sh/v3/covid-19/historical?lastdays=30"
        r = requests.get(url)
        j = r.json()
        res = []
        for country in j:
            country_name = country["country"]
            province_name = country["province"]

            df = pd.DataFrame(country["timeline"])

            df["countriesAndTerritories"] = country_name
            df["province"] = province_name
            df["dateRep"] = pd.to_datetime(df.index)
            res.append(df)
        all_data = pd.concat(res)
        all_data = all_data.groupby(["countriesAndTerritories", "dateRep"]).sum()
        all_data.index = all_data.index.rename(["countriesAndTerritories", "dateRep"])

        url = "https://disease.sh/v3/covid-19/countries"
        all_data.to_csv()
        r = requests.get(url)
        j = r.json()

        countries = pd.DataFrame(
            j, columns=["country", "countryInfo", "continent", "population"]
        )
        countries["countryterritoryCode"] = countries["countryInfo"].map(
            lambda x: x["iso3"]
        )
        countries["geoId"] = countries["countryInfo"].map(lambda x: x["iso2"])
        countries.drop(columns="countryInfo", inplace=True)
        countries.rename(
            columns={"country": "countriesAndTerritories", "population": "popData2019"},
            inplace=True,
        )
        countries.set_index("countriesAndTerritories", inplace=True)

        all_data = all_data.join(countries)

        all_data.to_csv(args.output)
    else:
        # Throws exception if failed.
        run(["wget", args.url, "-O", local_name]).check_returncode()

    return 0


###################################################################################################
# Body
###################################################################################################


if __name__ == "__main__":
    run_script()

###################################################################################################
# End of file
###################################################################################################
