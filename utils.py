#!/usr/bin/env python3
# coding: utf-8
###################################################################################################
#  Copyright Jonathan Lucas (c) 2020.
###################################################################################################
#
#    This file is part of cases.
#
#    cases is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    cases is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with cases.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################################

###################################################################################################
# Imports
###################################################################################################
import logging
import os
import sys
from itertools import product
from typing import List, Dict, Iterable, Tuple

import numpy as np
import pandas as pd
from matplotlib.cm import get_cmap
from numpy.polynomial.polynomial import polyfit
from pykalman import KalmanFilter

PLOTTING_COLOURS = get_cmap("Dark2").colors
PLOTTING_MARKERS = ".xv"

###################################################################################################
# Globals
###################################################################################################
_world_data = os.path.join("data", "world_data.csv")
_capacity_data = os.path.join("data", "uk_capacity.csv")

BAD_CHARS = " "
GOOD_CHARS = "_" * len(BAD_CHARS)


def CHAR_MAPPER(x):
    return x.translate(str.maketrans(BAD_CHARS, GOOD_CHARS))


def generate_colour_lookup(labels: Iterable) -> Dict:
    return {t: c for (t, c) in zip(labels, PLOTTING_COLOURS)}


def generate_colour_marker_lookup(labels: Iterable) -> Dict:
    return {
        t: {"color": c, "marker": m}
        for (t, (m, c)) in zip(labels, product(PLOTTING_MARKERS, PLOTTING_COLOURS))
    }


def days_elapsed(data: pd.DataFrame) -> pd.Series:
    elapsed = np.round((data.index - data.index[0]).total_seconds(), 0) / (24 * 3600.0)
    return elapsed


def read_uk_data(file: str = "data/uk_data.xlsx"):
    uk_data: pd.DataFrame = pd.read_excel(
        "data/uk_data.xlsx",
        index_col="DateVal",
        usecols=["DateVal", "CMODateCount", "DailyDeaths"],
    )
    uk_data.columns = ["cases", "deaths"]
    return uk_data


def read_uk_hospital_data(file: str = "data/UK_COVID-19_Press_Conference_Data.xlsx"):
    xl = pd.read_excel(
        file, sheet_name="People in Hospital (UK)", header=4, parse_dates=True
    )
    xl["Date"] = pd.to_datetime(xl["Date"], errors="coerce")
    xl = xl.dropna(subset=["Date"])
    xl = xl.set_index("Date")
    return xl


def read_owid_data(
        file: str, values: List[str] = ["cases", "deaths"], countries: List[str] = None
) -> Tuple[pd.DataFrame, pd.Series, List]:
    data = pd.read_csv(file)

    data["location"] = data.location.map(CHAR_MAPPER)
    data.date = pd.to_datetime(data.date)
    country_data = data.pivot(
        index="date", columns="location", values=values
    )
    country_data.columns.names = ["field", "country"]

    return country_data


def read_world_data(
        file: str, values: List[str] = ["cases", "deaths"], countries: List[str] = None
) -> Tuple[pd.DataFrame, pd.Series, List]:
    if file.endswith("csv"):
        data = pd.read_csv(file)
    else:
        data = pd.read_excel(file)
    # data.rename({data.columns[0]: "dateRep"}, axis="columns", inplace=True)
    data.dateRep = pd.to_datetime(data.dateRep, dayfirst=True)
    country_data = data.pivot(
        index="dateRep", columns="countriesAndTerritories", values=values
    )
    country_data.columns.names = ["case_type", "country"]
    country_data = country_data.diff()

    pop_data = data.groupby("countriesAndTerritories").first()["popData2019"]
    if countries:
        try:
            pop_data = pop_data[countries]
        except KeyError:
            logging.error(f"Unknown countries: {set(countries) - set(pop_data.index)}")
            sys.exit(-1)
        country_data = country_data.loc[:, (values, countries)]

    return country_data, pop_data, list(pop_data.index)


def read_us_data(
        file: str, values: List[str] = ["cases", "deaths"], states: List[str] = None
) -> Tuple[pd.DataFrame, pd.Series, List]:
    data = pd.read_csv(file, parse_dates=["date"])
    state_data = data.pivot(index="date", columns="state", values=values)

    state_data.columns.names = ["case_type", "state"]
    state_data = state_data.diff()

    if states:
        state_data = state_data.loc[:, (values, states)]
    else:
        states = list(state_data.columns.levels[1])

    return state_data, None, states


def read_uk_ons_data(
        file: str, values: List[str] = ["cases", "deaths"], states: List[str] = None
) -> Tuple[pd.DataFrame, pd.Series, List]:
    data = pd.read_csv(file, parse_dates=["date"])
    state_data = data.pivot_table(index="date", columns="region", values=values)
    state_data.columns.names = ["case_type", "region"]

    if states:
        state_data = state_data.loc[:, (values, states)]
    else:
        states = list(state_data.columns.levels[1])

    return state_data, None, states


def realign(data: pd.DataFrame, threshold) -> pd.DataFrame:
    realigned = {}
    for country in data:
        cases = data[country]
        cases = cases[cases.cumsum() > threshold]
        try:
            cases.index = days_elapsed(cases)
            realigned[country] = cases
        except IndexError:
            pass
    realigned = pd.DataFrame(realigned)

    return realigned


def get_trend(data, t):
    elapsed = days_elapsed(data)
    ylog = np.log(data)
    fit = np.polyfit(elapsed[-t:], ylog[-t:], deg=1)
    pred = np.poly1d(fit)
    return pred, fit


def rate(data):
    try:
        elapsed = days_elapsed(data)
    except AttributeError:
        elapsed = data.index

    coef, (resid, _, _, _) = polyfit(elapsed, data, deg=1, full=True)

    return coef, resid


def est_var(data):
    return rate(data)[1]


def remove_outliers(data, args):
    if len(data) > args.est_window and args.outlier_limit_sd > 0:
        data = data[data >= 0]
        series = np.log(data + 1)
        df = pd.DataFrame()
        df["sample_mean"] = series.rolling(args.rolling_mean).mean()
        df["sample_sd"] = series.rolling(args.rolling_mean).var(1) ** 0.5
        df = df.shift(1)
        df = df.bfill()
        df = df.ffill()

        df["upper_bound"] = df.sample_mean + df.sample_sd * args.outlier_limit_sd
        df["lower_bound"] = df.sample_mean - df.sample_sd * args.outlier_limit_sd
        df["log_data"] = series
        df["standardised"] = (series - df.sample_mean) / df.sample_sd
        df["data"] = data
        df["outliers"] = ((df.log_data < df.lower_bound) | (df.log_data > df.upper_bound)) & (df.data > 30)
        data = data[~df.outliers]
        # print(f"removed {df.outliers.sum()} outliers")

    return data


def filter_const_v(unfiltered, args):
    """
    Perform Kalman filter on data in log domain.
    :param series: Input data (assumed to be in the linear domain)
    :param est_window: Lookback window used to estimate measurement noise.
    :param model_cov_weight: transition matrix noise hyperparameter. Make this bigger to
        reduce filtering effect
    :return: Dataframe containing filtered data.
    """

    transition_matrix = np.array([[1, 1], [0, 1]])
    obs_matrix = np.eye(2, 2)
    trans_cov = args.model_cov_weight * np.array([[1 / 3, 1 / 2], [1 / 2, 1]])

    unfiltered = unfiltered.copy()
    unfiltered.name = "X_unfiltered"
    df = pd.DataFrame(unfiltered)

    try:
        rolling = unfiltered.rolling(f"{args.rolling_mean}D").mean()
    except ValueError:
        rolling = unfiltered.rolling(args.rolling_mean).mean()
    rolling = rolling.shift(-(args.rolling_mean // 2))
    df["X_rolling"] = rolling

    df["X_obs"] = np.log(rolling + 1)
    df["X_raw_log"] = np.log(unfiltered + 1)

    df["x_obs_covar_est"] = df["X_obs"].rolling(args.est_window).apply(est_var)
    df["x_obs_covar_est"] = df["x_obs_covar_est"].shift(-args.est_window // 2)
    df["x_obs_covar_est"].bfill(inplace=True)
    df["x_obs_covar_est"].ffill(inplace=True)

    df["delayed"] = df.X_raw_log.shift(periods=7, freq="D")
    df["delayed"].fillna(df.X_raw_log.iloc[0], inplace=True)
    df["V_obs"] = (df.X_raw_log - df.delayed) / 7
    obs_v_var = df.V_obs.rolling(7).var().mean()
    obs_covar = np.array([[df["x_obs_covar_est"].mean(), 0],
                          [0, obs_v_var]])
    obs = pd.DataFrame([df.X_obs, df.V_obs]).T
    obs.ffill(inplace=True)
    obs.dropna(inplace=True)
    kf = KalmanFilter(transition_matrices=transition_matrix,
                      transition_covariance=trans_cov,
                      observation_matrices=obs_matrix,
                      observation_covariance=obs_covar)
    if args.smooth:
        means, covars = kf.smooth(obs)
        X = means[:, 0]
        X_var = covars[:, 0, 0]
        V = means[:, 1]
        V_var = covars[:, 1, 1]

    else:

        # Set up initial means and covariances assuming perfect model fit.
        fit, _ = rate(df.X_obs.iloc[0: args.est_window])
        means = np.array([fit[0] - fit[1], fit[1]])
        vars = trans_cov
        X = []
        X_var = []
        V = []
        V_var = []

        for s, resid in zip(df.X_obs, df["x_obs_covar_est"]):
            means, vars = kf.filter_update(
                means, vars, np.array([s]), observation_covariance=np.array([resid])
            )
            X.append(means[0])
            X_var.append(vars[0, 0])
            V.append(means[1])
            V_var.append(vars[1, 1])

    log_result = pd.DataFrame(
        {
            "X": pd.Series(X, index=obs.index),
            "X_var": pd.Series(X_var, index=obs.index),
            "V": pd.Series(V, index=obs.index),
            "V_var": pd.Series(V_var, index=obs.index),
        }
    )

    result = np.exp(log_result)
    result["X_rolling"] = rolling

    result["X_upper"] = np.exp(log_result.X + (log_result.X_var ** 0.5) * 1.65)
    result["X_lower"] = np.exp(log_result.X - (log_result.X_var ** 0.5) * 1.65)
    result.loc[:, ["X", "X_upper", "X_lower"]] -= 1

    result["V_upper"] = np.exp(log_result.V + (log_result.V_var ** 0.5) * 1.65)
    result["V_lower"] = np.exp(log_result.V - (log_result.V_var ** 0.5) * 1.65)

    result.loc[:, ["V", "V_upper", "V_lower"]] = (result.loc[:, ["V", "V_upper", "V_lower"]] - 1) * 100
    log_result.columns = ["log_" + c for c in log_result.columns]
    result = result.merge(log_result, left_index=True, right_index=True)
    # Join because the indices are offset due to filter delay
    result = result.join(unfiltered, how="outer")
    # result.V = (result.V - 1) * 100
    # result.V_var = result.V_var * 100

    return result


def add_standard_args(parser):
    parser.add_argument(
        "-v",
        "--verbosity",
        type=int,
        choices=(0, 1, 2, 3, 4),
        default=2,
        help="Set verbosity level for debug (0=silent, 4=maximum chattiness)",
    )
    parser.add_argument(
        "-i", "--input_data", type=str, default=_world_data, help="Input data file."
    )
    parser.add_argument(
        "-c",
        "--capacity_data",
        type=str,
        default=_capacity_data,
        help="Hospital capacity data file.",
    )
    parser.add_argument(
        "-f", "--format", action="store", default="svg", help="Destination format."
    )
    parser.add_argument(
        "-S",
        "--smooth",
        action="store_true",
        default=False,
        help="Use Kalman smoother.",
    )
    parser.add_argument(
        "-R",
        "--rolling_mean",
        action="store",
        default=7,
        type=int,
        help="Apply n-day rolling mean (1=skip).",
    )
    parser.add_argument(
        "-T",
        "--case_type",
        action="append",
        default=[],
        choices=[
            "cases",
            "deaths",
            "cases_weekly",
            "deaths_weekly",
            "admissions",
            "ventilated",
            "occupancy",
        ],
        help="Destination format.",
    )
    parser.add_argument(
        "-l",
        "--start_at_level",
        action="store",
        default=10,
        type=float,
        help="Start plots when cumulative case count reaches this level.",
    )
    parser.add_argument(
        "-w",
        "--est_window",
        action="store",
        default=7,
        type=int,
        help="Number of days over which to estimate measurement noise, "
             "centered around the present day.",
    )
    parser.add_argument(
        "-m",
        "--model_cov_weight",
        action="store",
        default=0.00005,
        type=float,
        help="Model covariance weight (lower = more smoothing).",
    )
    parser.add_argument(
        "-OL",
        "--outlier_limit_sd",
        action="store",
        default=6,
        type=float,
        help="Remove data points based on #SDs from filtered mean (0=off.",
    )
    parser.add_argument("-o", "--output", type=str, help="Output directory.")
    parser.add_argument("country", nargs="*", help="Plot this country.")

###################################################################################################
# Body
###################################################################################################

###################################################################################################
# End of file
###################################################################################################
